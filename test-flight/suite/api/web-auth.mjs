
import TestFlight from "zunzun/test-flight/index.mjs"
const test = TestFlight.Test.test;
const test_async = TestFlight.Test.test_async;
const fail = TestFlight.fail;

const Skip = TestFlight.Skip;
const Sequence = TestFlight.Sequence;
const Assert = TestFlight.Assert;

import AES from 'zunzun/flypto/aes.mjs'
import AuthSession from 'zunzun/authing.ui/session.mjs'
import Fingerprint from 'zunzun/flyfp.os/index.mjs'


export default class WebAuthAPI {
  static async construct(cfg, tools, subject_socket) {
    try {
      const _this = new WebAuthAPI(cfg, tools, subject_socket);
      _this.session = await tools.fixtures["srv/authing"].Session(_this.test_cfg, tools);
      _this.authing_hook = await tools.hooks["srv/authing"]("Auth");
      _this.imap_client = await tools.fixtures["srv/authing"].IMAPClient(_this.test_cfg, tools);


      _this.browser = await tools.fixtures["srv/test-flight"].BrowserSession();
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg, tools, subject_socket) {
    this.cfg = cfg;
    this.tools = tools;
    this.subject_socket = subject_socket;

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test
        break;
      }
    }
  }

  async subject_data(data) {
    if (data.browser) {
      await this.browser.data(data.browser);
    } else {

    }
  }

  async wait_for_browser(browser_process) {
    const _this = this;
    return await new Promise((resolve) => {
      const wait_interval = setInterval(() => {
        if (_this.close_current_browser) {
          browser_process.kill('SIGTERM');
          _this.close_current_browser = false;
          clearInterval(wait_interval);
          resolve();
        }
      }, 333);
    });
  }

  async routine() {
    const seq = new Sequence();
    const _this = this;

    await this.browser.spawn();

    _this.authing_hook.cmd('call', 'empty_table', 'submitted_emails');
    _this.authing_hook.cmd('call', 'empty_table', 'accounts');
    _this.authing_hook.cmd('call', 'empty_table', 'sessions');

/*
    _this.authing_hook.cmd('call', 'empty_table', 'submitted_email');
    _this.authing_hook.cmd('call', 'empty_table', 'accounts');
    _this.authing_hook.cmd('call', 'empty_table', 'sessions');



    await seq.test_async('Gecko (Firefox)', async () => {
      const browser_process = Exec.spawn(`firefox --new-instance --devtools --new-window ${test_url}`);
      await _this.wait_for_browser(browser_process);
    });*/
/*
    _this.authing_hook.cmd('call', 'empty_table', 'submitted_email');
    _this.authing_hook.cmd('call', 'empty_table', 'accounts');
    _this.authing_hook.cmd('call', 'empty_table', 'sessions');

    await seq.test_async('Blink (Chromium)', async () => {
      const browser_process = Exec.spawn(`CHROMIUM_FLAGS="--disk-cache-dir=/dev/null --disk-cache-size=1" chromium --incognito --auto-open-devtools-for-tabs --new-window ${test_url}`);
      await _this.wait_for_browser(browser_process);
    });*/
/*
    _this.authing_hook.cmd('call', 'empty_table', 'submitted_email');
    _this.authing_hook.cmd('call', 'empty_table', 'accounts');
    _this.authing_hook.cmd('call', 'empty_table', 'sessions');

    await seq.test_async('WebKit (Epiphany)', async () => {
      const browser_process = Exec.spawn(`GSK_RENDERER=gl epiphany ${test_url}`);
      await _this.wait_for_browser(browser_process);
    });*/


    await seq.test_async('Public key availability', async () => {
      throw new Skip("Not implemented!");
    });


    let verification_code = undefined;
    await seq.test_async('Email submission', async () => {

      await _this.browser.test({
        fn: "email_submission",
        params: ["tester", "tester@subtledev.space"]
      });

      verification_code = await _this.imap_client.get_verification_code(_this.test_cfg.tester_imap);
    });

    await seq.test_async('Account creation', async () => {

      await _this.browser.test({
        fn: "account_creation",
        params: [verification_code, "123456"]
      });

    });


    await seq.test_async('Unauthenticated including', async () => {

      throw new Skip("Not implemented!");
    });


    await seq.test_async('Incorrect credentials', async () => {
      await _this.browser.navigate("/authing-test/sign-in/index.html");
      await _this.browser.test({
        fn: "incorrect_credentials",
        params: ["tester", "abcde"]
      });

      await _this.browser.reload();
      await _this.browser.test({
        fn: "incorrect_credentials",
        params: ["abcde", "123456"]
      });
    });


    await seq.test_async('Authentication', async () => {
      await _this.browser.reload();
      await _this.browser.test({
        fn: "authentication",
        params: ["tester", "123456"]
      });
      await _this.browser.confirm_path("/authing-test/account/index.html");
    });


    await seq.test_async('Authorization', async () => {
      await _this.browser.confirm_path("/authing-test/account/index.html");
    });

    await seq.test_async('Authorization with values', async () => {
      throw new Skip("Not implemented!");
    });

    await seq.test_async('Session destruction', async () => {
      await _this.browser.test({
        fn: "session_destruction"
      });
      await _this.browser.confirm_path("/authing-test/sign-in/index.html");
    });


    await seq.test_async('Reauthentication', async () => {
      await _this.browser.reload();
      await _this.browser.test({
        fn: "authentication",
        params: ["tester", "123456"]
      });
      await _this.browser.confirm_path("/authing-test/account/index.html");
    });

    await seq.test_async('Reauthorization', async () => {
      await _this.browser.confirm_path("/authing-test/account/index.html");
    });


    await seq.test_async('Authentication on unterminated session', async () => {
      await _this.browser.navigate("/authing-test/sign-in/index.html");
      await _this.browser.reload();
      await _this.browser.test({
        fn: "authentication",
        params: ["tester", "123456"]
      });
      await _this.browser.confirm_path("/authing-test/account/index.html");

    });

    _this.browser.kill();

    seq.throw();
  }


}
