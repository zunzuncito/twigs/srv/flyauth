
import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const test_async = FlyTest.Test.test_async;
const fail = FlyTest.fail;

const Skip = FlyTest.Skip;
const Sequence = FlyTest.Sequence;
const Assert = FlyTest.Assert;

import AES from 'zunzun/flypto/aes.mjs'
import AuthSession from 'zunzun/authing.ui/session.mjs'
import Fingerprint from 'zunzun/flyfp.os/index.mjs'


export default class AuthSecurity {
  static async construct(cfg, tools) {
    try {
      const _this = new AuthSecurity(cfg, tools);
      _this.session = await tools.fixtures["srv/authing"].Session(_this.test_cfg, tools);

      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg, tools) {
    this.cfg = cfg;
    this.tools = tools;

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test
        break;
      }
    }
  }

  async penetration() {
    const seq = new Sequence();
    const _this = this;

    await _this.session.get_pub_key();
    await _this.session.initialise();

    const values = {
      username: "qualphey",
      password: "123456"
    };
    await _this.session.authenticate(values);

    await seq.test_async('CSRF', async () => {
      throw new Skip("Not implemented!");
      return;
      const qstr = _this.session.authorize();
      const protected_url = `${_this.test_cfg.repo_url}${_this.test_cfg.protected_path}?${qstr}`;
      console.log(`GET > ${protected_url}`);
      const protected_html = await fetch(
        protected_url, {
          headers: {
            'Cookie': _this.session.cookie_store.get_header()
          }
        }
      ).then(res => res.text());

      console.log(protected_html);
      // TODO: redirect to some unauthenticated path
    });

    await seq.test_async('XSS', async () => {
      // TODO: authenticated request without proper cookies
      throw new Skip("Not implemented!");
    });


    await seq.test_async('Duplicate authenticated request (CSRF + XSS)', async () => {
      // TODO: authenticated request with proper cookies
      // Solution: start using one shot token for XSS
      // And: use herthbeat requests hearthbeat requests to update
      throw new Skip("Not implemented!");
    });

    seq.throw();
  }


}
