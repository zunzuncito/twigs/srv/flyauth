
import TestFlight from "zunzun/test-flight/index.mjs"
const test = TestFlight.Test.test;
const test_async = TestFlight.Test.test_async;
const fail = TestFlight.fail;

const Skip = TestFlight.Skip;
const Sequence = TestFlight.Sequence;
const Assert = TestFlight.Assert;

import AES from 'zunzun/flypto/aes.mjs'
import AuthSession from 'zunzun/authing.ui/session.mjs'
import Fingerprint from 'zunzun/flyfp.os/index.mjs'


export default class AuthAPI {
  static async construct(cfg, tools, subject_socket) {
    try {
      const _this = new AuthAPI(cfg, tools, subject_socket);
      _this.session = await tools.fixtures["srv/authing"].Session(_this.test_cfg, tools);
      _this.account = await tools.fixtures["srv/authing"].Account(_this.test_cfg, tools);
      _this.authing_hook = await tools.hooks["srv/authing"]("Auth");
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg, tools, subject_socket) {
    this.cfg = cfg;
    this.tools = tools;
    this.subject_socket = subject_socket;

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test
        break;
      }
    }
  }

  async routine() {
    const seq = new Sequence();
    const _this = this;

    await seq.test_async('Public key availability', async () => {
      await _this.session.get_pub_key();
      Assert.equal(_this.session.creds.pub_key.length, 272, "Invalid public key length!");

      _this.account.set_pub_key(_this.session.creds.pub_key); // set public key for further tests
    });

    await _this.authing_hook.cmd('call', 'empty_table', 'submitted_emails');
    await _this.authing_hook.cmd('call', 'empty_table', 'accounts');
    await _this.authing_hook.cmd('call', 'empty_table', 'sessions');

    await seq.test_async('Email submission', async () => {
      const status = await _this.account.submit_email();
      Assert.equal(status, 200, "Invalid response status!");
    });

    await seq.test_async('Account creation', async () => {
      const status = await _this.account.create_account();
      Assert.equal(status, 200, "Invalid response status!");
    });


    await seq.test_async('Unauthenticated including', async () => {
      const including_url = `${_this.test_cfg.repo_url}${this.test_cfg.hybrid_path}`;
      console.log(`GET > ${including_url}`);
      const hybrid_html = await fetch(including_url).then(res => res.text());

      const pubkey_match = hybrid_html.match(/const AUTHING_PUBLIC_KEY = `-----BEGIN PUBLIC KEY-----(.|\n)*-----END PUBLIC KEY-----\n`/gm);
      Assert.gt(pubkey_match.length, 0, `Public RSA key missing! Path: ${_this.test_cfg.repo_url}${this.test_cfg.hybrid_path}`);
    });


    await seq.test_async('Unauthorized', async () => {
      const unauthorized_url = `${_this.test_cfg.repo_url}${_this.test_cfg.protected_path}`;
      console.log(`GET > ${unauthorized_url}`);
      const pres = await fetch(unauthorized_url).then(async (res) => {
        return {
          status: res.status,
          text: await res.text()
        }
      });
      Assert.equal(pres.status, 403, `Invalid response code: ${pres.status}! Expected: 403`);
      Assert.equal(pres.text, "Forbidden", `Invalid response text: "${pres.text}"! Expected: "Forbidden"`);
    });


    let creds = {};

    await seq.test_async('Session initialization', async () => {
      await _this.session.initialise();

      // Cookie checks
      const client_key = _this.session.cookie_store.values.flyauth_client_key;
      Assert.string(client_key, "Invalid client key cookie format!");
      Assert.gt(client_key.length, 0, "Invalid client key cookie length!");

      console.log(_this.session.cookie_store.values);
      const enc_key = _this.session.cookie_store.values.flyauth_enc_keys;
      console.log(enc_key);
      Assert.string(enc_key, "Invalid encryption key cookie format!");
      Assert.gt(enc_key.length, 0, "Invalid encryption key cookie length!");

      const xss_token = _this.session.cookie_store.values.flyauth_anti_xss;
      Assert.string(xss_token, "Invalid XSS token cookie format!");
      Assert.gt(xss_token.length, 0, "Invalid XSS token cookie length!");


      // Local storage checks
      const fingerprint = _this.session.creds.fingerprint;
      Assert.string(fingerprint, "Invalid fingerprint format!");
      Assert.gt(fingerprint.length, 0, "Invalid fingerprint length!");

      const session_key = _this.session.creds.key;
      Assert.string(session_key, "Invalid session key format!");
      Assert.gt(session_key.length, 0, "Invalid session key length!");

      const iaes_key = _this.session.creds.iaes.key;
      Assert.string(iaes_key, "Invalid AES decryption key format!");
      Assert.gt(iaes_key.length, 0, "Invalid AES decryption key length!");

      const oaes_key = _this.session.creds.oaes.key;
      Assert.string(oaes_key, "Invalid AES encryption key format!");
      Assert.gt(oaes_key.length, 0, "Invalid AES encryption key length!");

      const csrf_token = _this.session.creds.csrf_secret;
      Assert.string(csrf_token, "Invalid CSRF token format!");
      Assert.gt(csrf_token.length, 0, "Invalid CSRF token length!");

      const tab_id = _this.session.creds.tab_id;
      Assert.number(tab_id, "Invalid tab id format!");

      const cks = _this.session.creds.cks;
      Assert.number(cks, "Invalid session checksum format!");
    });

    await seq.test_async('Incorrect credentials', async () => {
//      await _this.session.initialise();
      let resp = await _this.session.authenticate({
        username: "incorrect",
        password: _this.test_cfg.tester_account.password
      });
      Assert.equal(
        resp,
        "Unauthorized",
        "Invalid response on incorrect credentials"
      );

      await _this.session.initialise();
      resp = await _this.session.authenticate({
        username: _this.test_cfg.tester_account.user,
        password: "incorrect"
      });
      Assert.equal(
        resp,
        "Unauthorized",
        "Invalid response on incorrect credentials"
      );
    });


    await seq.test_async('Authentication', async () => {
      await _this.session.initialise();
      const values = {
        username: _this.test_cfg.tester_account.user,
        password: _this.test_cfg.tester_account.password
      };
      const resp = await _this.session.authenticate(values);
      Assert.not_equal(
        resp,
        "Unprocessable Entity",
        "Server response: Unprocessable Entity"
      );
      _this.session.decrypt_auth(resp);
      _this.session.creds.csrf_secret = _this.session.creds.csrf_secrets[0];
    });


    await seq.test_async('Authorization', async () => {
      let qstr = await _this.session.authorize();
      let protected_url = `${_this.test_cfg.repo_url}${_this.test_cfg.protected_path}?${qstr}`;
      console.log(`GET > ${protected_url}`);
      let resp = await fetch(
        protected_url, {
          headers: {
            'Cookie': _this.session.cookie_store.get_header()
          }
        } 
      );

      Assert.equal(resp.status, 200, 'Authorization failure!');
      _this.session.parse_html(await resp.text());

      qstr = await _this.session.authorize();
      protected_url = `${_this.test_cfg.repo_url}${_this.test_cfg.protected_path}?${qstr}`;
      console.log(`GET > ${protected_url}`);
      resp = await fetch(
        protected_url, {
          headers: {
            'Cookie': _this.session.cookie_store.get_header()
          }
        } 
      );

      Assert.equal(resp.status, 200, 'Authorization failure!');
      _this.session.parse_html(await resp.text());
    });

    await seq.test_async('Authorization with values', async () => {
      throw new Skip("Not implemented!");
    });

    await seq.test_async('Session destruction', async () => {
      await _this.session.destroy();

      // TODO validate the session destroyed
    });

    await seq.test_async('Session reinitialization', async () => {
      await _this.session.initialise();

      // Cookie checks
      const client_key = _this.session.cookie_store.values.flyauth_client_key;
      Assert.string(client_key, "Invalid client key cookie format!");
      Assert.gt(client_key.length, 0, "Invalid client key cookie length!");

      const enc_key = _this.session.cookie_store.values.flyauth_enc_keys;
      Assert.string(enc_key, "Invalid encryption key cookie format!");
      Assert.gt(enc_key.length, 0, "Invalid encryption key cookie length!");

      const xss_token = _this.session.cookie_store.values.flyauth_anti_xss;
      Assert.string(xss_token, "Invalid XSS token cookie format!");
      Assert.gt(xss_token.length, 0, "Invalid XSS token cookie length!");


      // Local storage checks
      const fingerprint = _this.session.creds.fingerprint;
      Assert.string(fingerprint, "Invalid fingerprint format!");
      Assert.gt(fingerprint.length, 0, "Invalid fingerprint length!");

      const session_key = _this.session.creds.key;
      Assert.string(session_key, "Invalid session key format!");
      Assert.gt(session_key.length, 0, "Invalid session key length!");

      const iaes_key = _this.session.creds.iaes.key;
      Assert.string(iaes_key, "Invalid AES decryption key format!");
      Assert.gt(iaes_key.length, 0, "Invalid AES decryption key length!");

      const oaes_key = _this.session.creds.oaes.key;
      Assert.string(oaes_key, "Invalid AES encryption key format!");
      Assert.gt(oaes_key.length, 0, "Invalid AES encryption key length!");

      const csrf_token = _this.session.creds.csrf_secret;
      Assert.string(csrf_token, "Invalid CSRF token format!");
      Assert.gt(csrf_token.length, 0, "Invalid CSRF token length!");

      const tab_id = _this.session.creds.tab_id;
      Assert.number(tab_id, "Invalid tab id format!");

      const cks = _this.session.creds.cks;
      Assert.number(cks, "Invalid session checksum format!");
    });

    await seq.test_async('Reauthentication', async () => {
      const values = {
        username: _this.test_cfg.tester_account.user,
        password: _this.test_cfg.tester_account.password
      };
      const resp = await _this.session.authenticate(values);
      Assert.not_equal(
        resp,
        "Unprocessable Entity",
        "Server response: Unprocessable Entity"
      );
      _this.session.decrypt_auth(resp);
    });


    seq.throw();
  }


}
