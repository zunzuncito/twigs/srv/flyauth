
import bcrypt from 'bcrypt'

export default class Auth {
  static async construct(sm) {
    const _this = new Auth(sm);
    _this.authing = sm.get_service_instance("authing");
    return _this;
  }

  constructor() {
  }

  async empty_table(table_name) {
    const target_table = this.authing.table(table_name);
    await target_table.delete("all");
  }

  async create_user(username, email, pwd) {
    const salt = bcrypt.genSaltSync(10);
    return (await this.authing.table("accounts").insert([
      `username`, `email`, `password`
    ], [`$1`, `$2`, `$3`], [
      username,
      email,
      bcrypt.hashSync(pwd, salt)
    ], 'RETURNING ID')).id
  }

  async grant_super_privileges(username) {
    await this.authing.table("accounts").update(['privileges = $1'], "username = $2", [['super'], username]);
  }

  async deny_super_privileges(username) {
    await this.authing.table("accounts").update([`privileges = array_remove(privileges, $1)`], "username = $2", ['super', username]);
  }
}
