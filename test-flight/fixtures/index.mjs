
import Session from "./auth/session.mjs"
import Account from "./auth/account.mjs"
import IMAPClient from "./auth/imap-client.mjs"


export default class Fixtures {
  static Session = Session
  static Account = Account
  static IMAPClient = IMAPClient
}
