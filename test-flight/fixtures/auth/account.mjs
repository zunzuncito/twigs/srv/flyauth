
import RSA from 'zunzun/flypto/rsa.mjs'

export default class Account {
  static async construct(cfg, tools) {
    const _this = new Account(cfg, tools);
    _this.imap_client = await tools.fixtures["srv/authing"].IMAPClient(_this.test_cfg, tools);
    return _this;
  }

  constructor(cfg, tools) {
    this.cfg = cfg;
    this.tools = tools;
    this.creds = {};

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test;
        break;
      }
    }

  }

  set_pub_key(pkey) {
    this.pkey = pkey;
  }

  async submit_email() {
    const rsa = new RSA(this.pkey.replace(/\\n/g, "\r\n"));

    console.log("SUBMIT", this.test_cfg.tester_account);

    const submit_email_url = `${this.test_cfg.repo_url}/flyauth/submit-email`;
    console.log(`PUT > ${submit_email_url}`);
    const resp = await fetch(submit_email_url, {
      method: "PUT",
      body: "data="+rsa.encrypt(JSON.stringify({
        username: this.test_cfg.tester_account.user,
        email: this.test_cfg.tester_imap.user
      }))
    });

    this.verification_code = await this.imap_client.get_verification_code(this.test_cfg.tester_imap);
    return resp.status;
  }

  async create_account() {
    const rsa = new RSA(this.pkey.replace(/\\n/g, "\r\n"));

    const signup_url = `${this.test_cfg.repo_url}/flyauth/signup`;
    console.log(`PUT > ${signup_url}`);
    const resp = await fetch(signup_url, {
      method: "PUT",
      body: `data=${rsa.encrypt(JSON.stringify({
        verification_code: this.verification_code,
        password: this.test_cfg.tester_account.password
      }))}`
    });
    return resp.status;
  }

}
