
import IMAP from 'zunzun/imap/index.mjs'

export default class IMAPClient {
  static async construct(cfg, tools) {
    const _this = new IMAPClient(cfg, tools);
    return _this;
  }

  constructor(cfg, tools) {
    this.cfg = cfg;
    this.tools = tools;
    this.creds = {};

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test;
        break;
      }
    }

  }

  async get_verification_code(tester_imap) {
    console.log(`Connecting to IMAP server: ${tester_imap.host}:${tester_imap.port}`);
    console.log("tester_imap", tester_imap);
    const imap = await IMAP.construct({
      user: tester_imap.user,
      password: tester_imap.password,
      host: tester_imap.host,
      port: tester_imap.port,
      tls: true,
      tls_options: {
        rejectUnauthorized: false // This will ignore invalid/self-signed certificates
      }
    });

    console.log(`Opening inbox...`);
    let nomail = true;
    const msgs = [];
    const uids = [];
    await new Promise((resolve) => {
      const nomail_interval = setInterval(async () => {
        if (nomail) {
          try {
            await imap.open_inbox("INBOX");
            for (let msg of imap.inbox) {
              msgs.push(msg);
              uids.push(msg.attrs.uid);
            }
            nomail = false;
          } catch (e) {
            console.log("No mail available! Retrying...");
          }
        } else {
          clearInterval(nomail_interval);
          resolve();
        }
      }, 1000);
    });

    console.log("Parsing verification code...");
    const verification_code = imap.inbox[0].body.match(/code\r\n(.*)\r\n/m)[1];

    console.log("Cleaning up inbox...");
    if (uids.length > 0) await imap.delete(uids);
    imap.end();

    console.log("CODE:", verification_code);
    return verification_code
  }
}
