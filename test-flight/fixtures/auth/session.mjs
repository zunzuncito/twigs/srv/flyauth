
import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const test_async = FlyTest.Test.test_async;
const fail = FlyTest.fail;

const Skip = FlyTest.Skip;
const Sequence = FlyTest.Sequence;
const Assert = FlyTest.Assert;

import AES from 'zunzun/flypto/aes.mjs'
import AuthSession from 'zunzun/authing.ui/session.mjs'
import CSRF_MG from 'zunzun/authing.ui/csrf/mg.mjs'
import Fingerprint from 'zunzun/flyfp.os/index.mjs'

const fp_base = 1.5;

class CookieStore {
  constructor() {
    this.values = {};
  }

  parse_response(response) {
    response.headers.forEach((value, name) => {
      if (name == 'set-cookie') {
        const cookie = value.split("=");
        this.values[cookie[0]] = cookie[1];
      }
    });
  }

  get_header() {
    let hstr = undefined;
    for (let cname in this.values) {
      if (!hstr) {
        hstr = `${cname}=${this.values[cname]}`
      } else {
        hstr += `; ${cname}=${this.values[cname]}`
      }
    }
    return hstr;
  }
}

export default class Session {
  static async construct(cfg, tools) {
    const _this = new Session(cfg, tools);
    return _this;
  }

  constructor(cfg, tools) {
    this.cfg = cfg;
    this.tools = tools;
    this.cookie_store = new CookieStore();
    this.creds = {};

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test;
        break;
      }
    }

  }

  async get_pub_key() {
    const pub_key_url = `${this.test_cfg.repo_url}/flyauth/public-key`;
    console.log(`GET > ${pub_key_url}`);
    this.creds.pub_key = await fetch(pub_key_url).then(res => res.text());
  }

  async initialise() {
    this.creds.fingerprint = Fingerprint.get(1.5).toString();

    const qstr = AuthSession.initialize(this.creds);
    const session_init_url = this.test_cfg.repo_url+'/flyauth/init-session';
    console.log(`POST > ${session_init_url} + ${qstr}`);
    const enc_sess_data = await fetch(session_init_url, {
      method: 'POST',
      body: `${qstr}`
    }).then(response => {
      this.cookie_store.parse_response(response);
      return response.text();
    });

    AuthSession.decrypt_init(enc_sess_data, this.creds);
  }

  async authenticate(values) {
    const qstr = AuthSession.authenticate(values, this.creds);
    const authenticate_url = this.test_cfg.repo_url+'/flyauth/signin';
    console.log(`POST > ${authenticate_url} + ${qstr}`);
    const response = await fetch(authenticate_url, {
      method: 'POST',
      body: `${qstr}`,
      headers: {
        'Cookie': this.cookie_store.get_header()
      }
    }).then(response => {
      this.cookie_store.parse_response(response);
      return response.text();
    });

    return response;
  }

  decrypt_auth(response) {
    AuthSession.decrypt_auth(response, this.creds);
/*    this.csrf_mg = new CSRF_MG(this.creds, new AES(this.creds.key), {
      authing_url: this.test_cfg.repo_url,
      header_fn: () => {
        return {
          'Cookie': this.cookie_store.get_header()
        };
      }
    });*/
  }

  async authorize(values) {
    if (this.csrf_mg) this.creds.csrf_secret = await this.csrf_mg.next();
    return AuthSession.credentials(this.creds, values);
  }

  parse_html(html) {
    const enc_creds = html.match(/const AUTHING_CREDENTIALS.*\n.*enc: '(.*)'/m)[1];
    const session_aes = new AES(this.creds.key);

    const fresh_creds = JSON.parse(session_aes.decrypt(enc_creds));

    if (!this.csrf_mg) {
      this.csrf_mg = new CSRF_MG(this.creds, new AES(this.creds.key), {
        authing_url: this.test_cfg.repo_url,
        header_fn: () => {
          return {
            'Cookie': this.cookie_store.get_header()
          };
        }
      });
    }

    this.csrf_mg.ots_list = fresh_creds.csrf_secrets;
  }

  async destroy() {
    const auth_str = await this.authorize();
    const destroy_url = `${this.test_cfg.repo_url}/flyauth/signout`;
    console.log("POST >", destroy_url, "+", auth_str);
    const resp = await fetch(destroy_url, {
      method: "post",
      body: `${auth_str}`,
      headers: {
        'Cookie': this.cookie_store.get_header()
      }
    });
  }
}
