

import FlyAuth from 'zunzun/authing.web/index.mjs'

const flyauth_div = document.querySelector("div.flyauth");

(async () => {
  try {
    const flyauth = new FlyAuth.SignUp({
      success_path: "/authing-test/sign-in/index.html"
    })
    flyauth_div.appendChild(flyauth.element);
  } catch (e) {
    console.error(e);
  }
})();
