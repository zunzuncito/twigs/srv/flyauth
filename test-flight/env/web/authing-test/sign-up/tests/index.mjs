import InSim from 'zunzun/flyhtml/in/sim.mjs'


let input = document.querySelector("div.flyauth input");

BROWSER_FLIGHT.add_test("email_submission", async (username, email) => {
  InSim.type(input, username);
  InSim.press_key(input, "Enter");

  input = document.querySelector("div.flyauth input");
  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (input.placeholder == "Email address") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });


  InSim.type(input, email);
  InSim.press_key(input, "Enter");

  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (input.placeholder == "Verification code") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });
});

BROWSER_FLIGHT.add_test("account_creation", async (verification_code, password) => {
  InSim.type(input, verification_code);
  InSim.press_key(input, "Enter");

  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (input.placeholder == "**password**") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });

  InSim.type(input, password);
  InSim.press_key(input, "Enter");

  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (input.placeholder == "**repeat**") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });

  InSim.type(input, password);
  InSim.press_key(input, "Enter");
});

BROWSER_FLIGHT.ready();

