
import InSim from 'zunzun/flyhtml/in/sim.mjs'


const input = document.querySelector("div.flyauth input");
const err_block = document.querySelector("div.flyauth div.err_msg");


BROWSER_FLIGHT.add_test("session_destruction", async () => {
  const sign_out_lnk = document.querySelector("a");
  sign_out_lnk.click();
  console.log("AUTHORIZATION TEST");

});

BROWSER_FLIGHT.ready();
