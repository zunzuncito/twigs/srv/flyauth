
import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'


const fp_base = 1.5;
(async () => {
  try {
    console.log("STARTING SIGNOUT");
    const dev_fp = Fingerprint.get(fp_base);
    const fp_salt = Fingerprint.get_salt();
    const cred_parser = new CredParser(dev_fp, fp_salt);
    const creds = await cred_parser.next();

//    await XHR.post('/flyauth/signout', enc_aes.encrypt(JSON.stringify(creds)));

    await fetch("/flyauth/signout", {
      method: "post",
      body: `cks=${creds.cks}&authorized=${creds.authorized}`
    });

    localStorage.removeItem("flyauth_session_key");
    window.location.href = "/authing-test/sign-in/index.html";

    setTimeout(() => {
      hide_loading_overlay();
    }, 3000);
 
  } catch (e) {
    console.error(e.stack);
  }
})();
