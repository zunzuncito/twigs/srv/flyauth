
import InSim from 'zunzun/flyhtml/in/sim.mjs'


const input = document.querySelector("div.flyauth input");
const err_block = document.querySelector("div.flyauth div.err_msg");


BROWSER_FLIGHT.add_test("incorrect_credentials", async (username, password) => {
  InSim.type(input, username);
  InSim.press_key(input, "Enter");

  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (input.type == "password") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });


  InSim.type(input, password);
  InSim.press_key(input, "Enter");
  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (err_block.innerText == "Invalid credentials!") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });
});

BROWSER_FLIGHT.add_test("authentication", async (username, password) => {
  InSim.type(input, username);
  InSim.press_key(input, "Enter");

  await new Promise((fulfil) => {
    const wait_interval = setInterval(() => {
      if (input.type == "password") {
        clearInterval(wait_interval);
        fulfil();
      }
    }, 100);
  });


  InSim.type(input, password);
  setTimeout(() => {
    InSim.press_key(input, "Enter");
  }, 1000);
});

BROWSER_FLIGHT.ready();
