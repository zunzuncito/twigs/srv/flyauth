
import FlyAuth from 'zunzun/authing.web/index.mjs'

const flyauth_div = document.querySelector("div.flyauth");

(async () => {
  try {
    const flyauth = new FlyAuth.SignIn({
      auth_cb: async (authz_params) => {

      },
      success_path: `/authing-test/account`
    });
    flyauth_div.appendChild(flyauth.element);
  } catch (e) {
    console.error(e);
  }
})();
