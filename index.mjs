
import LibFlyAuth from 'zunzun/authing/index.mjs'


export default class FlyAuth {
  static async start(sm, cfg) {
    try {
      const pg = sm.get_service_instance("pg");
      const flyweb = sm.get_service_instance("flyweb");
      const ctx_group = flyweb.route_mg.get_ctx_group("/");

      const authing_instance = await LibFlyAuth.construct(pg, ctx_group, cfg);

      return authing_instance;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
